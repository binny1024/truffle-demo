const ethers = require('ethers');
const fs = require("fs");

//调用合约的函数或者事件，不需要全部函数的定义都列出来
let abi = [
    "function mint(address player,string tokenURI) public returns (uint256)",
    "function tokenURI(uint256 tokenId) public view returns (string memory)",
];
 abi = [
    "function mint(uint256 _tokenId, address _owner, string memory _metadata) public",
    "function getNFTInfoById(uint256 _tokenid) public view returns (string memory, address)",
];

// Connect to the network
let provider = new ethers.providers.JsonRpcProvider(
    "https://node.lingjingchain.cn"
);

// 地址来自上面部署的合约
let contractAddress = "0xeDCC3cBa36cf47BC8085d295BA6E5c10e045a6Fb";

// 使用Provider 连接合约
let contract = new ethers.Contract(contractAddress, abi, provider);

// 钱包地址的私钥；生产环境注意保密，用完可以清空
let privateKey= fs.readFileSync("../.pri").toString().trim();
console.log(privateKey)
// 从私钥获取一个签名器 Signer
let wallet = new ethers.Wallet(privateKey, provider);

// 使用签名器创建一个新的合约实例，它允许使用可更新状态的方法
let contractWithSigner = contract.connect(wallet);

// 调用前面定义的abi交易函数，需要扣除交易费。 只读函数不用这种方式调用，可以在etherscan.io网上校验合约后直接调用
contractWithSigner
    .mint(
        1,
        "0x680341FF452F3276F7C132E39dB2b9c93b4c4197",
        "nodeddd"
    )
    .then(
        function (data) {
            if (data) {
                console.log("========data:");
                console.log(data);
            }
        },
        function (error) {
            if (error) {
                console.log("========error:");
                console.log(error);
            }
        }
    );
