# 合约编译

```shell
truffle compile --all
```

# 合约部署

## 只部署变动
```shell
truffle migrate --network  besu
```
## 重新部署
```shell
truffle migrate --network  besu --reset
```
```shell
truffle migrate --network  dev  --reset > dev.txt
```
# 测试
```shell
truffle test
```
